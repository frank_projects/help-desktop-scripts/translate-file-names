# Translate All Files
![Static Photo](./Assets/static.JPG)
#### Translate All Files gives you the ability to quickly translate all files and subfolders from a non English language to an English. This was created by Frank Haolun Li on 05.26.2019 in order to translate a series of Machine Learning videos that were named in Chinese.

#### During development of this tool I modified many existing open source packages in order to accomplish the task.

## Demo 
![Live Demo](./Assets/demo.gif)

## Dependencies:
  1. Python 3
  2. Selenium
  3. Numpy
  3. PhantomJS Web Driver
  4. Chrome Web Driver (optional)
  5. PyQt5


## Installation for Python 3
  Visit: https://www.python.org/downloads/ and download the version suitable for your System

## Installation for the rest
  Open Command Prompt (Windows) or Terminal (Linux) in the folder of this project and type in the following command. (You can do this by holding shift and right clicking on the folder and selecting open terminal/command prompt)

     pip install requirements.txt


## How to use?
  1.   Open Command Prompt (Windows) or Terminal (Linux) in the folder of this project and type in the following command. (You can do this by holding shift and right clicking on the folder and selecting open terminal/command prompt)

     python gui.py

  2. Enter the Language to be translated to in the "Language" Textbox (Enter full language name or the code)
  4. Select the folder you would like to translate
  5. Watch the process and enjoy :)


